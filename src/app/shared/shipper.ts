export class Shipper {
    constructor(){
        this.id=0;
        this.companyName="";
        this.phone="";
    }
    id: number;
    companyName: string;
    phone: string;
}
